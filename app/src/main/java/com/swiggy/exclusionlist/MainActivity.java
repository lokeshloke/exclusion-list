package com.swiggy.exclusionlist;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.swiggy.exclusionlist.adapters.VariantGroupAdapter;
import com.swiggy.exclusionlist.models.VariantsModel;
import com.swiggy.exclusionlist.network.RetroSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView list;
    VariantGroupAdapter adapter;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = findViewById(R.id.list);
        pb = findViewById(R.id.pb);

        findViewById(R.id.button).setOnClickListener(view -> fetchVariants());

    }

    private void displayProgress(boolean display){
        pb.setVisibility(display?View.VISIBLE:View.GONE);
    }

    private void fetchVariants() {
        displayProgress(true);
        Call<VariantsModel> call = RetroSingleton.api().getVariants();
        call.enqueue(new Callback<VariantsModel>() {
            @Override
            public void onResponse(Call<VariantsModel> call, Response<VariantsModel> response) {
                displayProgress(false);
                if(response.isSuccessful() && response.body() != null){
                    adapter = new VariantGroupAdapter(response.body().getVariants(),MainActivity.this);
                    list.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<VariantsModel> call, Throwable t) {
                displayProgress(false);
                Toast.makeText(getApplicationContext(),"Error Occurred",Toast.LENGTH_LONG).show();
            }
        });

    }
}
