package com.swiggy.exclusionlist.network;

import java.net.NetworkInterface;
import java.util.concurrent.Executors;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroSingleton {

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.myjson.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    static ApiInterface networkInterface;

    public static ApiInterface api(){
        if(networkInterface == null){
            networkInterface = retrofit.create(ApiInterface.class);
        }
        return networkInterface;
    }

}
