package com.swiggy.exclusionlist.network;

import com.swiggy.exclusionlist.models.VariantsModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("bins/3b0u2")
    Call<VariantsModel> getVariants();

}
