package com.swiggy.exclusionlist;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.swiggy.exclusionlist.models.Variation;

public class Utils {


    public static String getDisplayString(@NonNull Variation variation){

        StringBuilder sb = new StringBuilder();

        if(isValidString(variation.getName())){
            sb.append(variation.getName());
            sb.append(',');
        }

        sb.append("  Rs "+variation.getPrice());
        sb.append(',');

        sb.append(variation.inStock() ? "  InStock":"  Out of Stock");

        return sb.toString();


    }

    public static boolean isValidString(String s){
        return !TextUtils.isEmpty(s) && !s.equalsIgnoreCase("null");
    }

}
