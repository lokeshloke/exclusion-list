package com.swiggy.exclusionlist.models

import com.google.gson.annotations.SerializedName
import java.util.*

class Variants {

    @SerializedName("variant_groups")
    var variantGroups: List<VariantGroup>? = null
    @SerializedName("exclude_list")
    var excludeList: ArrayList<ArrayList<ItemIdentifier>>? = null
}

