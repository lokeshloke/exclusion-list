package com.swiggy.exclusionlist.models

import android.R.attr.*
import com.google.gson.annotations.SerializedName


class ItemIdentifier(groupId: Int, varId: Int) {

    constructor(groupId: Int,varId: Int,groupName: String,variationName: String) : this(groupId,varId) {
        this.groupName = groupName
        this.variationName = variationName
    }

    @SerializedName("group_id")
    var groupId: Int? = groupId
    @SerializedName("variation_id")
    var variationId: Int? = varId

    var groupName: String? = null
    var variationName: String? = null

    override fun equals(other: Any?): Boolean {

        if (other === this) return true
        if (other == null) return false

        if(other is ItemIdentifier){
            return other.groupId == groupId && other.variationId == variationId;
        }else{
            return false
        }
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = 1
        result = prime * result + if (groupId == null) 0 else groupId!!.hashCode()
                                  if (variationId == null) 0 else variationId!!.hashCode()

        return result
    }

}
