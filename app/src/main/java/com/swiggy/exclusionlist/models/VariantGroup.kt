package com.swiggy.exclusionlist.models

import com.google.gson.annotations.SerializedName

class VariantGroup {

    @SerializedName("group_id")
    var groupId: Int? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("variations")
    var variations: List<Variation>? = null

}

