package com.swiggy.exclusionlist.models

import com.google.gson.annotations.SerializedName

class Variation {

    @SerializedName("name")
    var name: String? = null
    @SerializedName("price")
    var price: Int = 0
    @SerializedName("default")
    var default: Int = 0
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("inStock")
    var inStock: Int = 0
    @SerializedName("isVeg")
    var isVeg: Int = 0

    fun inStock(): Boolean {
        return inStock == 1;
    }

}

