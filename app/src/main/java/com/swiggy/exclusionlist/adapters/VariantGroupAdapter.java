package com.swiggy.exclusionlist.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.swiggy.exclusionlist.R;
import com.swiggy.exclusionlist.Utils;
import com.swiggy.exclusionlist.models.ItemIdentifier;
import com.swiggy.exclusionlist.models.VariantGroup;
import com.swiggy.exclusionlist.models.Variants;
import com.swiggy.exclusionlist.models.Variation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class VariantGroupAdapter extends RecyclerView.Adapter<VariantGroupAdapter.VariantHolder> implements View.OnClickListener {

    List<VariantGroup> groupList;
    ArrayList<ArrayList<ItemIdentifier>> totalExclusionList;
    Activity mActivity;


    Map<ItemIdentifier,Integer> currExclusionMap = new HashMap<>(); // All the exclusions applied by existing selection
    Map<ItemIdentifier,Set<ItemIdentifier>> itemWiseExclusionMap = new HashMap<>() ;
    Map<Integer,Integer> groupMap = new HashMap<>(); // to locate the position of a group in the list in O(1)
    Map<Integer,ItemIdentifier> latestSelection = new HashMap<>(); // track the current selection (group,varaint)

    public VariantGroupAdapter(Variants variants, Activity mActivity){

        groupList = variants.getVariantGroups();
        totalExclusionList = variants.getExcludeList();
        this.mActivity = mActivity;

        prepareItemwiseExclusionList();
        
    }

    private void prepareItemwiseExclusionList(){
        if(totalExclusionList != null && !totalExclusionList.isEmpty()){
            for(List<ItemIdentifier> excCombo: totalExclusionList){
                Set<ItemIdentifier> currListCombos = new HashSet<>(excCombo);
                for(ItemIdentifier item: excCombo){
                    Set s = itemWiseExclusionMap.containsKey(item) ? itemWiseExclusionMap.get(item) : new HashSet<>();
                    s.addAll(currListCombos);
                    s.remove(item);
                    itemWiseExclusionMap.put(item,s);
                }

            }

        }
    }

    @Override
    public VariantHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new VariantHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull VariantHolder holder, int position) {

        VariantGroup group = groupList.get(position);
        List<Variation> variationList = group.getVariations();
        int groupId = group.getGroupId();
        groupMap.put(groupId,position);

        holder.titleTV.setText(group.getName());
        holder.variantGroup.removeAllViews();

        if(variationList != null && !variationList.isEmpty()){

            for(Variation variation: variationList){

                ItemIdentifier item = new ItemIdentifier(groupId,variation.getId(),group.getName(),variation.getName());

                RadioButton rb = new RadioButton(mActivity);
                rb.setTag(item);
                rb.setText(Utils.getDisplayString(variation));
                rb.setOnClickListener(VariantGroupAdapter.this);

                boolean shouldEnable = !((currExclusionMap != null && currExclusionMap.containsKey(item)) || !variation.inStock());
                rb.setEnabled(shouldEnable);

                // This item is already selected in this group, mark it as checked
                if(latestSelection.containsKey(groupId) && latestSelection.get(groupId).getVariationId() == variation.getId()){
                    rb.setChecked(true);
                }

                holder.variantGroup.addView(rb);
            }
        }



    }

    @Override
    public int getItemCount() {
        return (groupList != null  && !groupList.isEmpty() ) ? groupList.size() : 0;
    }

    @Override
    public void onClick(View view) {

        // Listener purpose is to handle item tagged radio button
        if(!(view.getTag() instanceof ItemIdentifier))
            return;

        ItemIdentifier selectedItem = (ItemIdentifier) view.getTag();
        int groupId = selectedItem.getGroupId();

        if(latestSelection.containsKey(groupId)){

            // A variant is already selected in this group,reduce exclusions imposed by it

            int prevChoiceInGroup = latestSelection.get(groupId).getVariationId();

            latestSelection.put(groupId,selectedItem);

            // invalidate to remove the previous selection of radio button
            notifyItemChanged(groupMap.get(groupId));


            Set<ItemIdentifier> prevChoiceExclusionSet = itemWiseExclusionMap.get(new ItemIdentifier(groupId,prevChoiceInGroup));
            modifyExclusionMap(prevChoiceExclusionSet,false);
            notifyItems(prevChoiceExclusionSet);

        }else{
            latestSelection.put(groupId,selectedItem);
        }

        Set<ItemIdentifier> set = itemWiseExclusionMap.get(selectedItem);
        modifyExclusionMap(set,true);  // Include the current selection's exclusion list

        notifyItems(currExclusionMap.keySet());

        displaySelection();

    }

    // latestselection will have at least 1 entry
    private void displaySelection(){
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<Integer,ItemIdentifier> entry: latestSelection.entrySet()){
            sb.append(entry.getValue().getGroupName()+"-"+entry.getValue().getVariationName()+",");
        }

        Toast.makeText(mActivity,sb.substring(0,sb.length()-1),Toast.LENGTH_SHORT).show();
    }

    private void modifyExclusionMap(Set<ItemIdentifier> itemSet,boolean include){
        if(itemSet != null){
            for (ItemIdentifier item: itemSet){
                if(currExclusionMap.containsKey(item)){
                    int count = currExclusionMap.get(item);
                    count += include ? 1:-1;
                    if(count > 0){
                        currExclusionMap.put(item,count);
                    }else{
                        currExclusionMap.remove(item);
                    }
                }else{
                    if(include)
                        currExclusionMap.put(item,1);
                }
            }
        }
    }

    private void notifyItems(Set<ItemIdentifier> list){
        if(list != null){
            for (ItemIdentifier item: list)
                if(groupMap.containsKey(item.getGroupId())){
                    notifyItemChanged(groupMap.get(item.getGroupId()));
                }
        }
    }


    public class VariantHolder extends RecyclerView.ViewHolder{

        TextView titleTV;
        RadioGroup variantGroup;

        public VariantHolder(View itemView) {
            super(itemView);
            titleTV = itemView.findViewById(R.id.title);
            variantGroup = itemView.findViewById(R.id.variantGroup);
        }
    }
}
